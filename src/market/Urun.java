/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package market;

/**
 *
 * @author seren
 */
public class Urun {
    String isim;
    int kod;
    double fiyat;
    int adet;
    
    public Urun(String isim, int kod, double fiyat){
        this.isim= isim;
        this.kod= kod;
        this.fiyat=fiyat;
        
        
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public int getKod() {
        return kod;
    }

    public void setKod(int kod) {
        this.kod = kod;
    }

    public double getFiyat() {
        return fiyat;
    }

    public void setFiyat(double fiyat) {
        this.fiyat = fiyat;
    }

    public int fiyatHesaplari(){
        return (int) (fiyat*adet);
    }

    @Override
    public String toString() {
        return "isim: "+ isim+ " Fiyat: "+ fiyat;
    }
    
                

  
}
